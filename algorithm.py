def Calculate(n, m, a, b):
    p = [None for x in range(n)]
    for i in range(n):
        p[b[i]-1] = a[i]-1

    odw = [False for x in range(n)]

    C = []
    c = 0
    for i in range(n):
        if not odw[i]:
            x = i
            C.append([])
            while not odw[x]:
                odw[x] = True
                C[c].append(m[x])
                x = p[x]
            c = c + 1

    MIN = min(m)
    SC = [sum(x) for x in C]
    MC = [min(x) for x in C]

    w = 0
    for i in range(c):
        met1 = SC[i] + (len(C[i]) - 2) * MC[i]
        met2 = SC[i] + MC[i] + (len(C[i]) + 1) * MIN
        w = w + min([met1, met2])
    return w
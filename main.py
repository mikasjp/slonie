from algorithm import Calculate

n = int(input())
m = list(map(int, input().rstrip().split(' ')))
a = list(map(int, input().rstrip().split(' ')))
b = list(map(int, input().rstrip().split(' ')))

w = Calculate(n, m, a, b)

print(w)
from algorithm import Calculate
import os
import time

for file in os.listdir('test'):
    if file.endswith('.in'):
        test_input = os.path.join('test', file)
        input = [list(map(int, x.rstrip().split(' '))) for x in open(test_input).read().rstrip().split('\n')]
        
        start = time.time()
        calculated = Calculate(input[0][0], input[1], input[2], input[3])
        end = time.time()

        test_output = test_input[0:-3] + '.out'
        expected = int(open(test_output).read().rstrip())
        
        result = ('OK' if expected == calculated else 'FAIL')
        format = '{0:.4f}'
        print(f'[{ result }] {test_input} ({ round(end-start, 4) } s)')

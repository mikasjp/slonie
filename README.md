# Słonie

## Usage

```sh
$ python main.py < test/slo1.in
30518
```

Where `test/slo1.in` is Your input in following format[:](https://szkopul.edu.pl/c/oboz10-14grupa__minus1/p/slo/18246/)

```txt
10
3015 4728 4802 4361 135 4444 4313 1413 4581 546
3 10 1 8 9 4 2 7 6 5
4 9 5 3 1 6 10 7 8 2
```

**The application does not verify input data!**

The first line is the number of elements separated by single spaces in the next three lines.
The second line is the weight values.
The third line is the initial setting of the elephants.
The fourth line is the target setting for elephants.

## Testing

There is also a tool for testing the implemented algorithm. It reads the test data from the test folder, calculates the result using the algorithm and compares the result of its operation with the expected value.
All you have to do is run the `test.py` script and wait for it to finish.

An example output of the tool's operation:

```sh
$ python test.py
[OK] test/slo9b.in (2.9658 s)
[OK] test/slo8b.in (3.5979 s)
[OK] test/slo10a.in (1.5532 s)
[OK] test/slo1ocen.in (0.0 s)
[OK] test/slo4.in (0.0082 s)
[OK] test/slo5.in (0.1061 s)
[OK] test/slo3ocen.in (0.0002 s)
[OK] test/slo1.in (0.0001 s)
[OK] test/slo6.in (1.2719 s)
[OK] test/slo2.in (0.0001 s)
[OK] test/slo10b.in (1.5127 s)
[OK] test/slo3.in (0.0008 s)
[OK] test/slo4ocen.in (0.5931 s)
[OK] test/slo7.in (1.405 s)
[OK] test/slo9a.in (3.5944 s)
[OK] test/slo8a.in (1.4394 s)
[OK] test/slo2ocen.in (0.0 s)
```

As you can see, the tool also measures the operation time of the algorithm.